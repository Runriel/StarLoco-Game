package org.starloco.locos.client.other;

import java.util.HashMap;
import java.util.Map;

public class Restriction {

    public static Map<Integer, Restriction> restrictions = new HashMap<>();
    //region
    public Map<String, Long> aggros = new HashMap<>();
	public long timeDeblo;
	public long timeAstrub;
    public boolean command = true;

	public Restriction() {
		this.timeDeblo = -1L;
		this.timeAstrub = -1L;
		this.aggros = new HashMap<String, Long>();
	}
	
    public static Restriction get(int id) {
        if (restrictions.get(id) != null)
            return restrictions.get(id);
        return new Restriction();
    }

    //endregion
}