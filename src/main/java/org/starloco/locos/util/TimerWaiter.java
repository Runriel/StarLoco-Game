package org.starloco.locos.util;

import org.starloco.locos.game.world.World;

import java.util.concurrent.*;
import java.util.*;

public class TimerWaiter {

    /**private static final ThreadFactory mapFactory = r -> new Thread(r) {{
        setName("Custom Thread Map");
        setDaemon(true);
    }};
    private static final ThreadFactory clientFactory = r -> new Thread(r) {{
        setName("Custom Thread client");
        setDaemon(true);
    }};
    private static final ThreadFactory fightFactory = r -> new Thread(r) {{
        setName("Custom Thread Fight");
        setDaemon(true);
    }};

    private static final ScheduledThreadPoolExecutor mapScheduler = new ScheduledThreadPoolExecutor(15, mapFactory);
    private static final ScheduledThreadPoolExecutor clientScheduler = new ScheduledThreadPoolExecutor(30, clientFactory);
    private static final ScheduledThreadPoolExecutor fightScheduler = new ScheduledThreadPoolExecutor(30, fightFactory);
**/
    private static int numberOfThread = 15 + 1;
    private static ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(numberOfThread);

    public static void update() {
        numberOfThread = World.world.getNumberOfThread() + 15;
        scheduler.shutdownNow();
        scheduler = Executors.newScheduledThreadPool(numberOfThread);
    }

    private final static Map<DataType, ScheduledThreadPoolExecutor> schedulerPools = new HashMap() {{
        put(DataType.MAP, null);
        put(DataType.CLIENT, null);
        put(DataType.FIGHT, null);
    }};

    public static void addNext(Runnable run, long time, TimeUnit unit, DataType scheduler) {
        TimerWaiter.scheduler.schedule(run, time, unit);
    }

    public static void addNext(Runnable run, long time, DataType scheduler) {
        TimerWaiter.addNext(run, time, TimeUnit.MILLISECONDS, scheduler);
    }

    public enum DataType {
        MAP,
        CLIENT,
        FIGHT
    }
}