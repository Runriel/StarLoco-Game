package org.starloco.locos.util.lang.type;

import org.starloco.locos.kernel.Config;
import org.starloco.locos.util.lang.AbstractLang;

/**
 * Created by Locos on 09/12/2015.
 */
public class French extends AbstractLang {

    public final static French singleton = new French();

    public static French getInstance() {
        return singleton;
    }

    public void initialize() {
        int index = 0;
        this.sentences.add(index, "Votre canal g�n�ral est d�sactiv�."); index++;
        this.sentences.add(index, "Les caract�res point virgule, chevrons et tild� sont d�sactiv�."); index++;
        this.sentences.add(index, "Tu dois attendre encore #1 seconde(s)."); index++;
        this.sentences.add(index, "Vous avez activ� le canal g�n�ral."); index++;
        this.sentences.add(index, "Vous avez d�sactiv� le canal g�n�ral."); index++;
        this.sentences.add(index, "Liste des membres du staff connect�s :"); index++;
        this.sentences.add(index, "Il n'y a aucun membre du staff connect�"); index++;
        this.sentences.add(index, "Vous n'�tes pas bloquer.."); index++;
        this.sentences.add(index, "<b>Kalypso 1.0.3</b>\nLe serveur est en ligne depuis #2 heure et #3 minutes."); index++;
        this.sentences.add(index, "\nIl y'a actuellement #1 joueurs connect�."); index++;
        this.sentences.add(index, "\nJoueurs uniques en ligne : #1"); index++;
        this.sentences.add(index, "\nRecord de connexion : #1"); index++;
        this.sentences.add(index, "Les commandes disponnibles sont :\n"
                + "<b>.infos</b> - Permet d'obtenir des informations sur le serveur.\n"
                + "<b>.deblo</b> - Permet de vous d�bloquer en vous t�l�portant � une cellule libre.\n"
                + "<b>.banque</b> - Permet d'ouvrir la banque.\n"
                + "<b>.maitre</b> - Permet de cr�er une escouade.\n"
                + "<b>.tp</b> - Permet de t�l�porter votre escouade.\n"
                + "<b>.astrub</b> - Permet de vous t�l�porter � astrub.\n"
                + "<b>.all</b> - Permet d'envoyer un message � tous les joueurs.\n"
                + "<b>.noall</b> - Permet de ne plus recevoir les messages du canal g�n�ral."); index++;
        //this.sentences.add(index, "Vous pouvez d�s � pr�sent voter, <b><a href='" + Config.getInstance().url + "'>clique ici</a></b> !");
    }
}
