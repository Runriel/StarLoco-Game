package org.starloco.locos.database.dynamics.data;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.starloco.locos.database.dynamics.AbstractDAO;
import org.starloco.locos.game.world.World;
import org.starloco.locos.game.world.World.titre;

import com.zaxxer.hikari.HikariDataSource;

public class Titre_Data extends AbstractDAO<titre>
{
	public Titre_Data(HikariDataSource dataSource)
	{
		super(dataSource);
	}

	public void load(Object obj)
	{
		return;
	}

	@Override
	public boolean update(titre titre)
	{
		return false;
	}
	
	public void load()
	{
		Result result = null;
		try
		{
			result = getData("SELECT * from titre");
			ResultSet RS = result.resultSet;
			while (RS.next())
			{
				titre SA = new titre(RS.getInt("color"), RS.getString("name"));
				World.add_Titre(RS.getInt("guid"), SA);
			}
		}
		catch (SQLException e)
		{
			super.sendError("Titre_Data load", e);
		}
		finally
		{
			close(result);
		}
	}
	
	public int gettitlecolor(int guid)
	{
		Result result = null;
		int color = 0;
		try
		{
			result = getData("SELECT `color` FROM `titre` WHERE `guid` = " + guid + ";");
			ResultSet RS = result.resultSet;
			boolean encontrado = RS.first();
			if (encontrado)
				color = RS.getInt("color");
		}
		catch (SQLException e)
		{
			super.sendError("Titre_Data.color exist", e);
		}
		finally
		{
			close(result);
		}
		return color;
	}
	
	public String gettitlename(int guid)
	{
		Result result = null;
		String name = "";
		try
		{
			result = getData("SELECT `name` FROM `titre` WHERE `guid` = " + guid + ";");
			ResultSet RS = result.resultSet;
			boolean encontrado = RS.first();
			if (encontrado)
				name = RS.getString("name");
		}
		catch (SQLException e)
		{
			super.sendError("Titre_Data.name exist", e);
		}
		finally
		{
			close(result);
		}
		return name;
	}
}