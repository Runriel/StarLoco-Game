package org.starloco.locos.kernel;

import com.typesafe.config.ConfigFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;

public class Config {

    public static final Config singleton = new Config();
    private static Logger logger = LoggerFactory.getLogger("Config");

    public final long startTime = System.currentTimeMillis();
    public boolean HALLOWEEN = false, NOEL = true, HEROIC = false;

	public String NAME, url, startMessage = "", colorMessage = "B9121B";
    public boolean autoReboot = true, allZaap = false, allEmote = false, onlyLocal = false;
    public int startMap = 0, startCell = 0;
    public int rateKamas = 1, rateDrop = 1, rateHonor = 1, rateJob = 1, rateFm = 1;
    public float rateXp = 1;

    public static Config getInstance() {
        return singleton;
    }

    public boolean load() {
        com.typesafe.config.Config fallback = ConfigFactory.load("references.conf");
        com.typesafe.config.Config conf = ConfigFactory.parseFile(new File("config.conf"))
                .withFallback(fallback);

        //Needed parameters
        try {
            Main.serverId = conf.getInt("server.id");
            Main.key = conf.getString("server.key");
            Main.gamePort = conf.getInt("server.port");
            Main.Ip = conf.getString("server.ip");

            Main.modDebug = conf.getBoolean("parameters.debug");
            Logging.USE_LOG = conf.getBoolean("parameters.logEnabled");
            Main.exchangePort = conf.getInt("exchange.port");
            Main.exchangeIp = conf.getString("exchange.ip");
            //BD
            Main.loginHostDB = conf.getString("database.login.host");
            Main.loginNameDB = conf.getString("database.login.name");
            Main.loginUserDB = conf.getString("database.login.user");
            Main.loginPassDB = conf.getString("database.login.password");
            Main.loginPortDB = conf.getString("database.login.port");
            //Game
            Main.hostDB = conf.getString("database.game.host");
            Main.nameDB = conf.getString("database.game.name");
            Main.userDB = conf.getString("database.game.user");
            Main.passDB = conf.getString("database.game.password");
            Main.portDB = conf.getString("database.game.port");


            this.NAME = "Localhost";
            this.url = "localhost/ip.txt";
            this.autoReboot = true;

            Main.useSubscribe = conf.getBoolean("gameplay.subscribeEnabled");
            this.startMap = conf.getInt("gameplay.startMap");
            this.startCell = conf.getInt("gameplay.startCell");
            this.allZaap = conf.getBoolean("gameplay.allZaapKnown");
            this.allEmote = conf.getBoolean("gameplay.allEmotesKnown");
            this.rateXp = conf.getInt("gameplay.rates.xp");
            this.rateDrop = conf.getInt("gameplay.rates.drop");
            this.rateJob = conf.getInt("gameplay.rates.job");
            this.rateKamas = conf.getInt("gameplay.rates.kamas");
            this.rateFm = conf.getInt("gameplay.rates.fm");
            this.startMessage = conf.getString("parameters.message");
            this.NOEL = conf.getBoolean("parameters.noel");
            this.HALLOWEEN = conf.getBoolean("parameters.halloween");
            this.HEROIC = conf.getBoolean("parameters.heroic");
        } catch (Exception e) {
            logger.error("Config is malformed or non existent {}", e.getMessage());
            return false;
        }

        return true;
    }
}