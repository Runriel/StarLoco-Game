package org.starloco.locos.command;

import org.starloco.locos.area.map.GameMap;
import org.starloco.locos.client.*;
import org.starloco.locos.client.other.*;
import org.starloco.locos.common.*;
import org.starloco.locos.database.*;
import org.starloco.locos.entity.Prism;
import org.starloco.locos.entity.monster.Monster;
import org.starloco.locos.entity.monster.Monster.MobGroup;
import org.starloco.locos.fight.Fight;
import org.starloco.locos.game.action.*;
import org.starloco.locos.game.world.*;
import org.starloco.locos.kernel.*;
import org.starloco.locos.util.TimerWaiter;
import org.starloco.locos.util.lang.Lang;

import java.text.*;
import java.util.*;
import java.util.concurrent.TimeUnit;

public class CommandPlayer {

    public final static String canal = "Kalypso";

	public static boolean analyse(Player player, String msg) {
        if (msg.charAt(0) == '.' && msg.charAt(1) != '.') {
        	if(msg.length() > 7 && msg.substring(1, 7).equalsIgnoreCase("banque")) {
                if (player.getFight() != null){
                    SocketManager.GAME_SEND_MESSAGE(player, "Vous ne pouvez pas utiliser cette commande en combat.");
                    return true;
                }
                boolean ok = false;
                        ok = true;

                if (ok) {
                    Database.getStatics().getPlayerData().update(player);
                    if (player.getDeshonor() >= 1) {
                        SocketManager.GAME_SEND_Im_PACKET(player, "183");
                        return true;
                    }
                    final int cost = player.getBankCost();
                    if (cost > 0) {
                        final long playerKamas = player.getKamas();
                        final long kamasRemaining = playerKamas - cost;
                        final long bankKamas = player.getAccount().getBankKamas();
                        final long totalKamas = bankKamas + playerKamas;
                        if (kamasRemaining < 0)//Si le joueur n'a pas assez de kamas SUR LUI pour ouvrir la banque
                        {
                            if (bankKamas >= cost) {
                                player.setBankKamas(bankKamas - cost); //On modifie les kamas de la banque
                            } else if (totalKamas >= cost) {
                                player.setKamas(0); //On puise l'enti�reter des kamas du joueurs. Ankalike ?
                                player.setBankKamas(totalKamas - cost); //On modifie les kamas de la banque
                                SocketManager.GAME_SEND_STATS_PACKET(player);
                                SocketManager.GAME_SEND_Im_PACKET(player, "020;"
                                        + playerKamas);
                            } else {
                                SocketManager.GAME_SEND_MESSAGE_SERVER(player, "10|"
                                        + cost);
                                return true;
                            }
                        } else
                        //Si le joueur a les kamas sur lui on lui retire directement
                        {
                            player.setKamas(kamasRemaining);
                            SocketManager.GAME_SEND_STATS_PACKET(player);
                            SocketManager.GAME_SEND_Im_PACKET(player, "020;"
                                    + cost);
                        }
                    }
                    SocketManager.GAME_SEND_ECK_PACKET(player.getGameClient(), 5, "");
                    SocketManager.GAME_SEND_EL_BANK_PACKET(player);
                    player.setAway(true);
                    player.setExchangeAction(new ExchangeAction<>(ExchangeAction.IN_BANK, 0));
                }
                return true;
        	} else if(msg.length() > 6 && msg.substring(1, 7).equalsIgnoreCase("maitre")) {
                if (player.getFight() != null){
                    SocketManager.GAME_SEND_MESSAGE(player, "Vous ne pouvez pas utiliser cette commande en combat.");
                    return true;
                }
        		if(player.isEsclave() == true) {
        			SocketManager.GAME_SEND_MESSAGE(player, "Action impossible, vous �tes un h�ro.");
					return true;
        		}
                
            	if(player.get_maitre() != null)
            	{
            	
                	player.get_maitre()._esclaves.forEach(esclave -> esclave.setEsclave(false));
            		player.set_maitre(null);
            		SocketManager.GAME_SEND_MESSAGE(player, "Commande d�sactiv�e.");
            		
            	}else if(player.get_maitre() != null && player.isEsclave() == true)
                	{
                	
                    	player.get_maitre()._esclaves.forEach(esclave -> esclave.setEsclave(false));
                		player.set_maitre(null);
                		SocketManager.GAME_SEND_MESSAGE(player, "Commande d�sactiv�e.");
                		
                	}else{
    				player.set_maitre(new Maitre(player));
    				if(player.get_maitre() != null){
    					SocketManager.GAME_SEND_MESSAGE(player, "Commande activ�e, vous avez "+player.get_maitre().getEsclaves().size()+" h�ros. Fa�te .tp pour t�l�porter votre escouade.");
    				}else
    					SocketManager.GAME_SEND_MESSAGE(player, "Aucun h�ro n'a �t� trouv�.");
            	}
    			return true;
        	} else if(command(msg, "tp")) {
                if(System.currentTimeMillis() - player.getGameClient().timeLastTP < 10000) {
                    SocketManager.GAME_SEND_MESSAGE(player, "Cette commande est disponible toute les 10 secondes.");
                    return true;
                }
                if (player.getCurMap().haveMobFix()) {
                    SocketManager.GAME_SEND_MESSAGE(player, "Vous ne pouvez pas utiliser cette commande en donjon.");
                    return true;
                }
                if (player.isInDungeon()) {
                    SocketManager.GAME_SEND_MESSAGE(player, "Vous ne pouvez pas utiliser cette commande en donjon.");
                    return true;
                }
                if (player.getFight() != null){
                    SocketManager.GAME_SEND_MESSAGE(player, "Vous ne pouvez pas utiliser cette commande en combat.");
                    return true;
                }
                if (player.getExchangeAction() != null){
                    SocketManager.GAME_SEND_MESSAGE(player, "Vous ne pouvez pas utiliser cette commande car vous �tes occuper.");
                    return true;
                }
    		    if(player.get_maitre() != null){
    		    player.getGameClient().timeLastTP = System.currentTimeMillis();
    			player.get_maitre().teleportAllEsclaves();
    			SocketManager.GAME_SEND_MESSAGE(player, "Vous avez t�l�porter "+player.get_maitre().getEsclaves().size()+" h�ros.");
    			}
    		    else
    			SocketManager.GAME_SEND_MESSAGE(player, "Aucun h�ro n'a �t� trouv� pour la t�l�portation.");
    			return true;
    		} else if (command(msg, "all") && msg.length() > 5) {
                if (player.isInPrison())
                    return true;
                if (player.noall) {
                    SocketManager.GAME_SEND_MESSAGE(player, Lang.get(player, 0), "C35617");
                    return true;
                }
                if(player.getGroupe() == null && System.currentTimeMillis() - player.getGameClient().timeLastTaverne < 10000) {
                    SocketManager.GAME_SEND_MESSAGE(player, Lang.get(player, 2).replace("#1", String.valueOf(10 - ((System.currentTimeMillis() - player.getGameClient().timeLastTaverne) / 1000))), "C35617");
                    return true;
                }

                player.getGameClient().timeLastTaverne = System.currentTimeMillis();

                String prefix = "<font color='#C35617'>(" + canal + ") <b><a href='asfunction:onHref,ShowPlayerPopupMenu," + player.getName() + "'>" + player.getName() + "</a></b>";

                Logging.getInstance().write("AllMessage", "[" + (new SimpleDateFormat("HH:mm").format(new Date(System.currentTimeMillis()))) + "] : " + player.getName() + " : " + msg.substring(5, msg.length() - 1));

                final String message = "Im116;" + prefix + "~" + msg.substring(5, msg.length() - 1).replace(";", ":").replace("~", "").replace("|", "").replace("<", "").replace(">", "") + "</font>";

                World.world.getOnlinePlayers().stream().filter(p -> !p.noall).forEach(p -> p.send(message));
                Main.exchangeClient.send("DM" + player.getName() + "|" + getNameServerById(Main.serverId) + "|" + msg.substring(5, msg.length() - 1).replace("\n", "").replace("\r", "").replace(";", ":").replace("~", "").replace("|", "").replace("<", "").replace(">", "") + "|");
                return true;
            } else if (command(msg, "noall")) {
                if (player.noall) {
                    player.noall = false;
                    SocketManager.GAME_SEND_MESSAGE(player, Lang.get(player, 3), "C35617");
                } else {
                    player.noall = true;
                    SocketManager.GAME_SEND_MESSAGE(player, Lang.get(player, 4), "C35617");
                }
                return true;
            } else if (msg.length() > 6 && msg.substring(1, 7).equalsIgnoreCase("astrub")) {
    				if (player.isInPrison()) {
    					return true;
    				}
    				if (player.cantTP()) {
    					return true;
    				}
    				if (player.getFight() != null) {
    					return true;
    				}
        			player.teleport((short) 7411, 311);
    				return true;
            } else if (command(msg, "deblo")) {//180min
                if (player.isInPrison())
                    return true;
                if (player.cantTP())
                    return true;
                if (player.getFight() != null)
                    return true;
                if(player.getCurCell().isWalkable(true)) {
                    SocketManager.GAME_SEND_MESSAGE(player, Lang.get(player, 7));
                    return true;
                }
                player.teleport(player.getCurMap().getId(), player.getCurMap().getRandomFreeCellId());
                return true;
            } else if (command(msg, "infos")) {
                long uptime = System.currentTimeMillis()
                        - Config.getInstance().startTime;
                int jour = (int) (uptime / (1000 * 3600 * 24));
                uptime %= (1000 * 3600 * 24);
                int hour = (int) (uptime / (1000 * 3600));
                uptime %= (1000 * 3600);
                int min = (int) (uptime / (1000 * 60));
                uptime %= (1000 * 60);
                int sec = (int) (uptime / (1000));
                int nbPlayer = Main.gameServer.getClients().size();
                //int nbPlayerIp = Main.gameServer.getPlayersNumberByIp();

                String mess = Lang.get(player, 8).replace("#1", String.valueOf(jour)).replace("#2", String.valueOf(hour)).replace("#3", String.valueOf(min)).replace("#4", String.valueOf(sec));
                if (nbPlayer > 0)
                    mess +=  Lang.get(player, 9).replace("#1", String.valueOf(nbPlayer));
                /*if (nbPlayerIp > 0)
                    mess +=  Lang.get(player, 10).replace("#1", String.valueOf(nbPlayerIp));*/
                SocketManager.GAME_SEND_MESSAGE(player, mess);
                return true;
            } else {
                SocketManager.GAME_SEND_MESSAGE(player, Lang.get(player, 12));
                return true;
            }
        }
        return false;
    }

    private static boolean command(String msg, String command) {
        return msg.length() > command.length() && msg.substring(1, command.length() + 1).equalsIgnoreCase(command);
    }

    private static String getNameServerById(int id) {
        switch(id) {
            case 13: return "Silouate";
            case 19: return "Allister";
            case 22: return "Oto Mustam";
            case 1: return "Jiva";
            case 37: return "Nostalgy";
            case 4001: return "Alma";
            case 4002: return "Aguabrial";
        }
        return "Unknown";
    }
}