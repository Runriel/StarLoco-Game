package org.starloco.locos.job;

import org.starloco.locos.client.Player;
import org.starloco.locos.common.SocketManager;
import org.starloco.locos.util.TimerWaiter;

public class JobCraft {

    public Player player;
    public Thread thread;
    public TimerWaiter waiter = new TimerWaiter();
    public JobAction jobAction;
    private int time = 0;
    private boolean itsOk = true;

    public JobCraft(JobAction jobAction, Player player) {
        this.jobAction = jobAction;
        this.player = player;

        this.thread = new Thread(() -> {
            try { Thread.sleep(1200); } catch(Exception ignored) { }
            if (itsOk) jobAction.craft(false, -1);
            try { Thread.sleep(1200); } catch(Exception ignored) { }
            if (!itsOk) repeat(time, time, player);
        });
        this.thread.start();
    }

    public void setAction(int time) {
        this.time = time;
        this.jobAction.broken = false;
        this.itsOk = false;
    }

    public void repeat(final int time, final int _time, final Player P) {
        final int j = time - _time;
        this.jobAction.player = P;
        this.jobAction.isRepeat = true;
        if (jobAction.broke || jobAction.broken || P.getExchangeAction() == null
                || !P.isOnline()) {
            if (P.getExchangeAction() == null)
                jobAction.broken = true;
            if (P.isOnline())
                SocketManager.GAME_SEND_Ea_PACKET(jobAction.player, jobAction.broken ? "2" : "4");
            end();
            return;
        } else {
            jobAction.ingredients.putAll(jobAction.lastCraft);
            SocketManager.GAME_SEND_EA_PACKET(jobAction.player, _time + "");
            jobAction.craft(jobAction.isRepeat, j);
        }

        if (_time <= 0)
            this.end();
        else
            TimerWaiter.addNext(() -> repeat(time, (_time - 1), P), 1000, null);
    }

    public void end() {
        SocketManager.GAME_SEND_Ea_PACKET(this.jobAction.player, "1");
        if (!this.jobAction.data.isEmpty())
            SocketManager.GAME_SEND_EXCHANGE_MOVE_OK_FM(this.jobAction.player, 'O', "+", this.jobAction.data);
        this.jobAction.ingredients.clear();
        this.jobAction.lastCraft.clear();
        this.jobAction.isRepeat = false;
        this.jobAction.setJobCraft(null);
        this.jobAction.resetCraft();
        this.jobAction.broken = false;
        this.thread.interrupt();
    }
}