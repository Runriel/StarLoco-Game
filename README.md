# StarLoco Game

Cette partie contient un game fonctionnel pour le serveur StarLoco.

Pour s'en servir il faut avoir peupler lancer le login avant tout
L'ensemble de la configuration se fait par le fichier config.conf

Le serveur peut être lancé :
  * soit par maven (recommandé) avec la commande 
```sh
mvn compile exec:java
```
  * soit par le jar 
```sh
java -jar login.jar
```

*Petits conseils*
  * *Sous linux, il vaut mieux changer les ports par défaut qui sont < 1024 et donc réservés pour le root* 
  * *Il vaut mieux utiliser deux base distinctes pour les donnees bien qu'actuellement on n'en profite pas tellement car le game et le login demande un accès aux deux"*

**Recompiler les sources**
Le projet étant sous maven il suffit de faire (avec maven correctement installé) 

```sh
mvn clean compile assembly:single
```

**Free Software, Hell Yeah!**
